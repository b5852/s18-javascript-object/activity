// console.log("Hello World!");


/*
	3. Create a trainer object using object literals.
*/

let trainer = {
/*
	
	4. Initialize/add the following trainer object properties:
	Name (String)
	Age (Number)
	Pokemon (Array)
	Friends (Object with Array values for properties)

*/
		name: "Ash Ketchum",
		age: "10",
		pokemon: ["Pikachu", "Eevee", "Snorlax", "Charmander"],
		friends: {
			heonn: ["May", "Max"],
			kanto: ["brock", "Misty"]
		},

/*
	5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
*/
		talk: function()
		{
		console.log("Pikachu! I choose you!")
		}
}

/*
	6. Access the trainer object properties using dot and square bracket notation.
*/
console.log("Result of dot notation: ");
console.log(trainer.name);


console.log("Result of the square notation: ");
console.log(trainer['pokemon']);

/*
	7. Invoke/call the trainer talk object method.
*/
trainer.talk();

/*
	8. Create a constructor for creating a pokemon with the following properties:
	Name (Provided as an argument to the constructor)
	Level (Provided as an argument to the constructor)
	Health (Create an equation that uses the level property)
	Attack (Create an equation that uses the level property)
	
*/


// Object Constructor Notation
function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	// Method

	this.tackle = function(target){
					//pokemoObject			// targetPokemon
		console.log(this.name + " takled " + target.name);
		// targetHealth - pokemonAttack = newHealth
		// 16 - 16 = 0
		console.log( target.name + "'s health is reduced to " + (target.level - this.level) );

		// call faint method if the target pokemon's health is less than or equal to zero.
		if ((target.level - this.level) < 0) {
			target.faint();	
		}
	}
	this.faint = function(){

		console.log(this.name + " fainted.")
	}
}

let pikachu = new Pokemon("Pikachu", 16);

let rattata = new Pokemon("Rattata", 8);
console.log(rattata);

// pikachu.tackle(rattata);
rattata.tackle(pikachu);



















